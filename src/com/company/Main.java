package com.company;
import java.util.*;
import com.company.Student;

public class Main {
    public static void main(String[] args) {
        System.out.println("====== List ======");
        List<String> list = new ArrayList();
        list.add("B");
        list.add("D");
        list.add("C");
        list.add("A");
        list.add("F");
        list.add("E");

        //Sort the list alphabetically
        Collections.sort(list);

        //Remove item in the sixth position
        list.remove(5);

        //Prints each item in the list
        for (Object str : list) {
            System.out.println((String)str);
        }



        System.out.println("====== Set ======");
        Set set = new TreeSet();
        set.add("B");
        set.add("D");
        set.add("C");
        set.add("A");
        set.add("E");
        set.add("E"); //Sets can oly contain unique elements.

        for (Object str : set) {
            System.out.println((String)str);
        }

        System.out.println("====== Queue ======");
        Queue queue = new PriorityQueue();
        queue.add("B");
        queue.add("D");
        queue.add("C");
        queue.add("A");
        queue.add("E");
        queue.add("F");

        System.out.println("QUEUE SIZE = " + queue.size());


        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("QUEUE SIZE = " + queue.size());

        System.out.println("====== Map ======");
        Map map = new HashMap();
        map.put(1,"B");
        map.put(2,"D");
        map.put(3,"C");
        map.put(4,"A");
        map.put(5,"E");
        map.put(4,"F");

        for (int i = 1; i < 6; i++) {
            String result = (String)map.get(i);
            System.out.println(result);
        }

        System.out.println("====== Map  2 ======");
        Map map2 = new HashMap();
        map2.put("name","Daniel");
        map2.put("age",20);
        map2.put("phone","55 84 99837-7877");

        System.out.println("Name: " + map2.get("name"));
        System.out.println("Age: " + map2.get("age"));
        System.out.println("Phone: " + map2.get("phone"));


        System.out.println("====== List using Generics ======");
        List<Student> myList = new LinkedList<Student>();
        myList.add(new Student("Daniel Melo", 25));
        myList.add(new Student("Amanda Johson", 20));

        for (Student student : myList) {
            System.out.println(student);
        }


        List<String> listExption = new ArrayList();

        try{
            listExption.add("this is the first value");
            System.out.println(listExption.get(1));    
        }catch (IndexOutOfBoundsException e){
            System.out.println(e.getMessage());
        }


    }
}
